const compose = require('koa-compose')
const Peppa = require('./peppa')

const {$pre, $getParagraphs, deleteTrailingLinks, extract, writeParagraphs, writeTitle, omit} = require('./wares')

const url = 'https://www.setn.com/News.aspx?NewsID=467015'

const peppa = new Peppa([$pre])

peppa.use(extract({
	title: $ => $('h1').text(),
	content: $ => $('#Content1').html()
}))
peppa.use(omit({title:['台湾','中国','大陆','党']}))
peppa.use(deleteTrailingLinks(['更多精彩分析', '更多报导']))
peppa.use($getParagraphs)
peppa.use(writeParagraphs('./output/test.txt'))
peppa.use(writeTitle('./output/title.txt'))
peppa.crawl(url).then(ctx => {
	console.log(ctx.paragraphs)
})

// const $deleteTrailingLinks = deleteTrailingLinks(['更多精彩分析', '更多报导'])

// let wares = [$pre]
// const context = {url}

// wares.push(async (ctx, next) => {
// 	ctx.title = ctx.$('h1').text()
// 	ctx.content = ctx.$('#Content1').html()
// 	await next()
// })

// wares.push(async (ctx, next) => {
// 	const $ = ctx.$
// 	const $content = $(ctx.content)
// 	const paragraphs = $content.map((i, p) => $(p).text()).get().filter(text => text.length>100)
// 	ctx.paragraphs = paragraphs
// 	await next()
// })

// wares.push($deleteTrailingLinks)
// compose(wares)(context).then(val => {
// 	console.log(context.paragraphs)
// })