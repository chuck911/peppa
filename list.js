const compose = require('koa-compose')
const Peppa = require('./peppa')
const {
	$pre,
	$getParagraphs,
	deleteTrailingLinks,
	extract,
	linksExtract,
	addUrlJobs,
	writeParagraphs,
	$getNextpage,
	writeTitle,
	omit
} = require('./wares')

const Queue = require('bull')
const categoryQueue = new Queue('setn-category')
const listQueue = new Queue('setn-list')
const articleQueue = new Queue('setn-article')


const peppa1 = new Peppa([$pre])

peppa1.use(linksExtract('#owlFamous .imgcaption-box>a'))
peppa1.use(addUrlJobs(listQueue))

// peppa1.crawl('https://www.setn.com/Catalog_Column.aspx?SubCateID=39')
// 	.then(ctx => {
// 		console.log(ctx.links)
// 	})

const peppa2 = new Peppa([$pre])
peppa2.use(linksExtract('.newsimg-area-item-2>a'))
peppa2.use(addUrlJobs(articleQueue))
peppa2.use($getNextpage)
peppa2.use(async (ctx, next) => {
	const jobOpt = {attempts:3, backoff:10000, timeout:20000}
	await listQueue.add({url: ctx.nextPage, name: ctx.nextPage}, jobOpt)
	await next()
})

const peppa3 = new Peppa([$pre])

peppa3.use(extract({
	title: $ => $('h1').text(),
	content: $ => $('#Content1').html()
}))
peppa3.use(omit({title:['台湾','中国','大陆','党']}))
peppa3.use(deleteTrailingLinks(['更多精彩分析', '更多报导']))
peppa3.use($getParagraphs)
peppa3.use(writeParagraphs('./output/p.txt'))
peppa3.use(writeTitle('./output/title.txt'))

categoryQueue.process(job => {
	return peppa1.crawl(job.data.url)
		.then(ctx => console.log('peppa1 '+ctx.url))
})

listQueue.process(job => {
	return peppa2.crawl(job.data.url)
		.then(ctx => console.log('peppa2 '+ctx.url))
})

articleQueue.process(job => {
	return peppa3.crawl(job.data.url)
		.then(ctx => console.log('peppa3 '+ctx.url))
})

const cateIds = [39,71,81,38,91,68,36,48,72,37]
Promise.all(
	cateIds
		.map(id => 'https://www.setn.com/Catalog_Column.aspx?SubCateID='+id)
		.map(url => categoryQueue.add({url, name:url}))
).then(()=>{
	console.log('init jobs')
})