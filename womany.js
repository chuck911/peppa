const Queue = require('bull')
const marked = require('marked')
const needle = require('needle')
const dayjs = require('dayjs')

const Peppa = require('./peppa')
const {
	$needle,
	$minimize,
    $readability,
	$turndown,
	$deleteEmojis
} = require('./wares')

const queue = new Queue('womany')

const peppa = new Peppa([$needle, $minimize, $deleteEmojis, $readability, $turndown])

peppa.use(async (ctx, next) => {
	ctx.article.title = ctx.article.title.replace('｜女人迷 Womany', '').trim()
    ctx.mark = ctx.mark.replace(/图片[｜来][^\n]+/g,'')
    ctx.mark = ctx.mark.replace(/womany 编[^\n]+/g,'')
    ctx.mark = ctx.mark.replace(/台湾/g,'中国')
    ctx.article.title = ctx.article.title.replace(/台湾/g,'中国')
    const days = Math.floor(Math.random()*50)
    ctx.date = dayjs().subtract(days, 'days').unix()
	await next()
})

peppa.use(async (ctx, next)=>{
    const api = 'http://www.qqmingmen.com/collect/ApiUserHuochetou/articleAdd'
	let content = marked(ctx.mark)
	const res = await needle('post', api, {
		password: 'qiu9iuM1mi',
		title: ctx.article.title,
		content,
		uid: '9f6ec18e0af263e92e1538ef',
        cid: 3,
        publish_time: ctx.date
    })
    const obj = JSON.parse(res.body)
    ctx.submitUrl = obj.data&&obj.data.id ? `http://www.qqmingmen.com/article/${obj.data.id}.html` : null
    ctx.res = obj
    await next()
})

peppa.use(async (ctx, next)=>{
    const api = 'http://data.zz.baidu.com/urls?site=www.qqmingmen.com&token=RlATKhNLSwKLpxa9&type=mip'
    if (ctx.submitUrl) {
        const res = await needle('post', api, ctx.submitUrl, {
            headers: {'Content-Type':'text/plain'}
        })
        console.log(res.body)
    }
    await next()
})

queue.process(job => {
	return peppa.crawl(job.data.url)
		.then(ctx => {
            console.log(ctx.res)
            return ctx.res
        })
})

// queue.add({url: 'https://cn.womany.net/read/article/3598'})
