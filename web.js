const path = require('path')
const Koa = require('koa')
const Router = require('koa-router')
const koaBody = require('koa-body')
const koaNunjucks = require('koa-nunjucks-2')
const marked = require('marked')
const needle = require('needle')
const OpenCC = require('opencc')
const opencc = new OpenCC('tw2sp.json')
const sites = require('./cms-sites.json')
const app = new Koa()
const router = new Router()
const Peppa = require('./peppa')
const {
	$needle,
	$minimize,
    $readability,
	$turndown,
	$deleteEmojis
} = require('./wares')

const peppa = new Peppa([$needle, $minimize, $deleteEmojis, $readability, $turndown])

peppa.use(async (ctx, next) => {
	ctx.article.title = ctx.article.title.replace('｜女人迷 Womany', '').trim()
	ctx.mark = ctx.mark.replace(/图片[｜来][^\n]+/g,'')
	await next()
})

app.use(koaNunjucks({
	ext: 'njk',
	path: path.join(__dirname, 'views'),
	nunjucksConfig: {}
}))

router.get('/', async (ctx) => {
	await ctx.render('index')
})

router.post('/', koaBody(), async (ctx) => {
    const url = ctx.request.body.url
    console.log(url)
    const {article, mark} = await peppa.crawl(url)
	await ctx.render('index', {article, url, mark, sites})
})

router.post('/post',koaBody(), async (ctx) => {
	let {title, md, cid, iconv, site} = JSON.parse(ctx.request.body)
	const siteInfo = sites[site]
	if (!siteInfo) {ctx.body = '目标网站未注册'; return}
	
	const api = `http://${siteInfo.domain}/collect/ApiUserHuochetou/articleAdd`
	let content = marked(md)
	if (iconv) {
		content = opencc.convertSync(content)
		title = opencc.convertSync(title)
	}
	const res = await needle('post', api, {
		password: siteInfo.password,
		title,
		content,
		// uid: '9f6ec18e0af263e92e1538ef',
		cid
	})
	// console.log(ctx.request.body.title)
	ctx.body = res.body
})

app.use(router.routes())
	.use(router.allowedMethods())


app.listen(3138)