const compose = require('koa-compose')

module.exports = class Peppa {
	constructor(wares = []) {
		this.wares = wares
	}

	use(fn) {
		this.wares.push(fn)
	}

	crawl(url) {
		const context = {url}
		return compose(this.wares)(context)
			.then(() => context)
	}
}