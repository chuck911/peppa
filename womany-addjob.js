const Queue = require('bull')
const queue = new Queue('womany')

;(async ()=>{
    for (let i = 0; i < 10000; i++) {
        const id = 10000+i
        await queue.add({url: `https://cn.womany.net/read/article/${id}`, name:id})
        console.log(i)
    }
})().then()